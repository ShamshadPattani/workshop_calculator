package com.example.admin.a2number;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    float a,b;
    float c;
 EditText v1,v2;
 TextView rslt,t1,t2;
 Button addbtn;
 @Override
   protected void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
     setContentView(R.layout.activity_main);
     v1=findViewById(R.id.value1);
     v2=findViewById(R.id.value2);
     rslt=findViewById(R.id.result);
     t1=findViewById(R.id.textView);
     t2=findViewById(R.id.textView2);
    }

   public void add(View view) {
    a= Float.parseFloat(v1.getText().toString());
    b=Float.parseFloat(v2.getText().toString());
    c=a+b;
     rslt.setVisibility(View.VISIBLE);
    rslt.setText(Float.toString(c));
    }

}
